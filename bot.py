
from flask import Flask, request, jsonify
from slackclient import SlackClient
import json
import os
import urllib.parse

config = \
    { "client_id":      os.environ["SLACK_CLIENT_ID"]
    , "client_secret":  os.environ["SLACK_CLIENT_SECRET"]
    , "oauth_scope":    os.environ["SLACK_BOT_SCOPE"]
    , "verification":   os.environ["SLACK_VERIFICATION"]
    , "team":           os.environ["SLACK_TEAM"]
    , "host":           os.environ["FLASK_HOST"]
    , "initialized":    False
    , "token_file":     "tokens.json"
    }

app = Flask(__name__)

@app.route("/begin_auth", methods=["GET"])
def pre_install():
    redirect_url = "{}/finish_auth".format(config["host"])
    return '''
        <a href="https://slack.com/oauth/authorize?scope={0}&client_id={1}&redirect_uri={2}">
            Add to Slack
        </a>
    '''.format(config["oauth_scope"], config["client_id"], urllib.parse.quote_plus(redirect_url))

@app.route("/finish_auth", methods=["GET", "POST"])
def post_install():
    # Retrieve the auth code from the request params
    auth_code = request.args['code']

    # An empty string is a valid token for this request
    sc = SlackClient("")

    # Request the auth tokens from Slack
    auth_response = sc.api_call(
        "oauth.access",
        client_id=config["client_id"],
        client_secret=config["client_secret"],
        code=auth_code
    )

    config["access_token"] = auth_response["access_token"]
    config["bot_access_token"] = auth_response["bot"]["bot_access_token"] if "bot" in auth_response else None
    config["initialized"] = True

    tokens = {}
    tokens["access_token"] = config["access_token"]
    tokens["bot_token"] = config["bot_access_token"]

    with open(config["token_file"], "w") as f:
        json.dump(tokens, f, indent=4)

    return "Auth complete!"

@app.route("/machines", methods=["POST"])
def get_machines():
    if check_post():
        machines = {
            "response_type": "ephemeral",
            "text": "Argboga: börk"
        }
        return jsonify(machines)
    else:
        fail = {
            "response_type": "in_channel",
            "text": "<sad trombone>"
        }
        return jsonify(fail)

def check_post():
    verification = request.form["token"] if "token" in request.form else None
    team = request.form["team_domain"] if "team_domain" in request.form else None
    return (config["verification"] == verification) and (config["team"] == team)

def init():
    try:
        with open(config["token_file"]) as f:
            tokens = json.load(f)
            config["access_token"] = tokens["access_token"]
            config["bot_token"] = tokens["bot_token"]
            config["initialized"] = True

    except (FileNotFoundError, KeyError):
        pass

init()

